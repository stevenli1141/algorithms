#include <iostream>
#include <vector>
#include <unordered_map>
#include <list>
#include <queue>

typedef std::pair<int, int> pair;

// Dijsktra
// Implement graph as an unordered_map
int dijkstra (int source, int dest, std::unordered_map<int, std::vector<pair> > G) {
    std::unordered_map<int, int> nodes;
    std::unordered_map<int, std::list<int> > seq;
    std::unordered_map<int, bool> visited;
    nodes[source] = 0;
    seq[source] = std::list<int> { source };
    std::priority_queue<pair, std::vector<pair>, std::greater<pair> > q;
    q.push(pair(0, source));
    while (!q.empty()) {
        int current = q.top().second;
        q.pop();
        for (pair& edge : G[current]) {
            int t = edge.first;
            int w = edge.second;

            if (t == dest) {
                auto l = seq[current];
                l.push_back(t);
                seq[t] = l;

                // Print route to stdout
                for (auto iter = seq[t].begin(); iter != seq[t].end(); ) {
                    std::cout << *iter;
                    if (++iter != seq[t].end()) {
                        std::cout << ", ";
                    }
                }
                std::cout << std::endl;

                return nodes[current] + w;
            }
            // Find all adjacent nodes to current and push into heap
            if (visited.count(t) == 0 && (nodes.count(t) == 0 || nodes[t] > nodes[current] + w )) {
                nodes[t] = nodes[current] + w;
                q.push(pair(nodes[current] + w, t));

                auto l = seq[current];
                l.push_back(t);
                seq[t] = l;
            }
        }
        visited[current] = true;
    }
    return -1;
}


/**Input specification
 * n (number of nodes)
 * k (number of undirected edges)
 * for i in range(k):
 *   s t w (s: source, t: target, w: weight)
 * s d (s: source, d: dest)
 * 
 * Output:
 * Sequence of nodes separated by commas followed by distance if there is a path
 * -1 otherwise
 */

int main() {
    int n, k;
    std::cin >> n >> k;
    std::unordered_map<int, std::vector<pair> > G;
    for (int i = 0; i < k; ++i) {
        int s, t, w;
        std::cin >> s >> t >> w;
        if (G.count(s) == 0) {
            G[s] = std::vector<pair>();
        }
        G[s].push_back(pair(t, w));
        G[t].push_back(pair(s, w));
    }
    int s, d;
    std::cin >> s >> d;
    int r = dijkstra(s, d, G);
    std::cout << r << std::endl;

    return 0;
}
