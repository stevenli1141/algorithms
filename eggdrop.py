def eggdrop(k, n):
    """
    Egg drop on n floors with k eggs available
    """
    dp = [[0 for i in range(k+1)] for i in range(n+1)]

    for i in range(1,n+1):
        for j in range(1,k+1):
            dp[i][j] = dp[i-1][j] + dp[i-1][j-1] + 1
            if dp[i][j] >= n:
                return i

k, n = int(input()), int(input())
print(eggdrop(k, n))
