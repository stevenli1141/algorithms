class Item:
    def __init__(self, w, c):
        self.weight = w
        self.cost = c

def knapsack(items, weight):
    """
    0,1-knapsack problem
    """
    n = len(items)
    if n == 0: return 0
    dp = [[0 for i in range(n)] for i in range(w+1)]

    for w in range(1, weight):
        for i in range(n):
            if i == 0:
                dp[w][i] = items[0].cost if w >= items[0].weight else 0
                continue
            dp[w][i] = max(dp[w][i-1], dp[w-items[i].weight][i-1] + items[i].cost)
    return dp[w][n-1]

n = int(input())
items = []
for i in range(n):
    item = Item(int(input()), int(input()))
    items.append(item)
w = int(input())
print(knapsack(items, w))
