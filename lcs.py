def lcs(x, y):
    """
    Longest common subsequence of strings x and y
    """
    m, n = len(x), len(y)
    if m * n == 0: return 0
    dp = [[0 for j in range(n+1)] for i in range(m+1)]
    for i in range(1,m+1):
        for j in range(1,n+1):
            if x[i-1] == y[j-1]:
                dp[i][j] = dp[i-1][j-1] + 1
            else:
                dp[i][j] = max(dp[i-1][j], dp[i][j-1])
    return dp[m][n]

x = input()
y = input()
print(lcs(x, y))
